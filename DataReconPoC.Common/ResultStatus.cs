﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataReconPoC.Common
{
    public enum ResultStatus
    {
        Unknown,
        Error,
        ConfidenceWarning,
        ValidationWarning,
        Success
    }
}
