﻿using Leadtools.Forms.Auto;
using Leadtools.Forms.Processing;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataReconPoC.Common
{
    public class ProcessingResult
    {
        [BsonId]
        public string Id { get; set; }

        public string Path { get; set; }

        public int OverallConfidence { get; set; }

        public DateTime TimeStamp { get; set; }

        public ElementResult<string> FormIdentifier { get; set; }

        public ElementResult<string> OrderNumber { get; set; }

        public ElementResult<bool?> Form_Refuse_Deceased { get; set; }

        public ElementResult<bool?> Form_Refuse_DaVitaDischarge { get; set; }

        public ElementResult<bool?> Form_Refuse_TempAbsence { get; set; }

        public ElementResult<string> Medication1_ID { get; set; }

        public ElementResult<bool?> Medication1_Accept { get; set; }

        public ElementResult<bool?> Medication1_Refuse { get; set; }

        public ElementResult<bool?> Medication1_Refuse_Discontinued { get; set; }

        public ElementResult<bool?> Medication1_Refuse_Overstock { get; set; }

        public ElementResult<bool?> Medication1_Refuse_Unknown { get; set; }

        public ElementResult<string> Medication2_ID { get; set; }

        public ElementResult<bool?> Medication2_Accept { get; set; }

        public ElementResult<bool?> Medication2_Refuse { get; set; }

        public ElementResult<bool?> Medication2_Refuse_Discontinued { get; set; }

        public ElementResult<bool?> Medication2_Refuse_Overstock { get; set; }

        public ElementResult<bool?> Medication2_Refuse_Unknown { get; set; }

        public ElementResult<string> Medication3_ID { get; set; }

        public ElementResult<bool?> Medication3_Accept { get; set; }

        public ElementResult<bool?> Medication3_Refuse { get; set; }

        public ElementResult<bool?> Medication3_Refuse_Discontinued { get; set; }

        public ElementResult<bool?> Medication3_Refuse_Overstock { get; set; }

        public ElementResult<bool?> Medication3_Refuse_Unknown { get; set; }

        public ElementResult<string> Medication4_ID { get; set; }

        public ElementResult<bool?> Medication4_Accept { get; set; }

        public ElementResult<bool?> Medication4_Refuse { get; set; }

        public ElementResult<bool?> Medication4_Refuse_Discontinued { get; set; }

        public ElementResult<bool?> Medication4_Refuse_Overstock { get; set; }

        public ElementResult<bool?> Medication4_Refuse_Unknown { get; set; }

        public ElementResult<string> Medication5_ID { get; set; }

        public ElementResult<bool?> Medication5_Accept { get; set; }

        public ElementResult<bool?> Medication5_Refuse { get; set; }

        public ElementResult<bool?> Medication5_Refuse_Discontinued { get; set; }

        public ElementResult<bool?> Medication5_Refuse_Overstock { get; set; }

        public ElementResult<bool?> Medication5_Refuse_Unknown { get; set; }

        public ElementResult<string> Medication6_ID { get; set; }

        public ElementResult<bool?> Medication6_Accept { get; set; }

        public ElementResult<bool?> Medication6_Refuse { get; set; }

        public ElementResult<bool?> Medication6_Refuse_Discontinued { get; set; }

        public ElementResult<bool?> Medication6_Refuse_Overstock { get; set; }

        public ElementResult<bool?> Medication6_Refuse_Unknown { get; set; }

        public ElementResult<string> Medication7_ID { get; set; }

        public ElementResult<bool?> Medication7_Accept { get; set; }

        public ElementResult<bool?> Medication7_Refuse { get; set; }

        public ElementResult<bool?> Medication7_Refuse_Discontinued { get; set; }

        public ElementResult<bool?> Medication7_Refuse_Overstock { get; set; }

        public ElementResult<bool?> Medication7_Refuse_Unknown { get; set; }

        public ElementResult<string> Medication8_ID { get; set; }

        public ElementResult<bool?> Medication8_Accept { get; set; }

        public ElementResult<bool?> Medication8_Refuse { get; set; }

        public ElementResult<bool?> Medication8_Refuse_Discontinued { get; set; }

        public ElementResult<bool?> Medication8_Refuse_Overstock { get; set; }

        public ElementResult<bool?> Medication8_Refuse_Unknown { get; set; }

        [BsonIgnore]
        [JsonConverter(typeof(StringEnumConverter))]
        public ResultStatus OverallStatus
        {
            get
            {
                if (OverallConfidence == 0 || Form_Status == ResultStatus.Error || Medication1_Status == ResultStatus.Error || Medication2_Status == ResultStatus.Error || Medication3_Status == ResultStatus.Error ||
                    Medication4_Status == ResultStatus.Error || Medication5_Status == ResultStatus.Error || Medication6_Status == ResultStatus.Error || Medication7_Status == ResultStatus.Error || Medication8_Status == ResultStatus.Error)
                {
                    return ResultStatus.Error;
                }
                if (OverallConfidence <= 90)
                {
                    return ResultStatus.ConfidenceWarning;
                }
                if (Form_Status == ResultStatus.ValidationWarning || Medication1_Status == ResultStatus.ValidationWarning || Medication2_Status == ResultStatus.ValidationWarning || Medication3_Status == ResultStatus.ValidationWarning ||
                    Medication4_Status == ResultStatus.ValidationWarning || Medication5_Status == ResultStatus.ValidationWarning || Medication6_Status == ResultStatus.ValidationWarning || Medication7_Status == ResultStatus.ValidationWarning || Medication8_Status == ResultStatus.ValidationWarning)
                {
                    return ResultStatus.ValidationWarning;
                }
                if (Form_Status == ResultStatus.Success && Medication1_Status == ResultStatus.Success && Medication2_Status == ResultStatus.Success && Medication3_Status == ResultStatus.Success &&
                    Medication4_Status == ResultStatus.Success && Medication5_Status == ResultStatus.Success && Medication6_Status == ResultStatus.Success && Medication7_Status == ResultStatus.Success && Medication8_Status == ResultStatus.Success)
                {
                    return ResultStatus.Success;
                }
                return ResultStatus.Unknown;
            }
        }
        
        [BsonIgnore]
        [JsonConverter(typeof(StringEnumConverter))]
        public ResultStatus Form_Status
        {
            get
            {

                if (OrderNumber == null || OrderNumber.Model == null || Form_Refuse_Deceased == null || !Form_Refuse_Deceased.Model.HasValue ||
                    Form_Refuse_TempAbsence == null || !Form_Refuse_TempAbsence.Model.HasValue || Form_Refuse_DaVitaDischarge == null || !Form_Refuse_DaVitaDischarge.Model.HasValue)
                {
                    return ResultStatus.Error;
                }
                if  ((Form_Refuse_Deceased.Model.Value || Form_Refuse_TempAbsence.Model.Value || Form_Refuse_DaVitaDischarge.Model.Value) &&
                    (
                    (Form_Refuse_Deceased.Model.Value && Form_Refuse_TempAbsence.Model.Value) ||
                    (Form_Refuse_Deceased.Model.Value && Form_Refuse_DaVitaDischarge.Model.Value) ||
                    (Form_Refuse_TempAbsence.Model.Value && Form_Refuse_DaVitaDischarge.Model.Value)
                    ))
                {
                    return ResultStatus.ValidationWarning;
                }
                return ResultStatus.Success;
            }
        }
        
        [BsonIgnore]
        [JsonConverter(typeof(StringEnumConverter))]
        public ResultStatus Medication1_Status 
        {
            get
            {
                if (Medication1_ID == null || Medication1_ID.Model == null || Medication1_Accept == null || !Medication1_Accept.Model.HasValue || Medication1_Refuse == null || !Medication1_Refuse.Model.HasValue ||
                    Medication1_Refuse_Discontinued == null || !Medication1_Refuse_Discontinued.Model.HasValue || Medication1_Refuse_Overstock == null || !Medication1_Refuse_Overstock.Model.HasValue ||
                    Medication1_Refuse_Unknown == null || !Medication1_Refuse_Unknown.Model.HasValue)
                {
                    return ResultStatus.Error;
                }
                if ((Medication1_Accept.Model.Value && Medication1_Refuse.Model.Value) ||
                    (!Medication1_Accept.Model.Value && !Medication1_Refuse.Model.Value && !(Form_Refuse_Deceased.Model.Value || Form_Refuse_TempAbsence.Model.Value || Form_Refuse_DaVitaDischarge.Model.Value)) ||
                    (Medication1_Accept.Model.Value && (Medication1_Refuse_Discontinued.Model.Value || Medication1_Refuse_Overstock.Model.Value || Medication1_Refuse_Unknown.Model.Value)) ||
                    (Medication1_Refuse.Model.Value && !(Medication1_Refuse_Discontinued.Model.Value || Medication1_Refuse_Overstock.Model.Value || Medication1_Refuse_Unknown.Model.Value)))
                {
                    return ResultStatus.ValidationWarning;
                }
                return ResultStatus.Success;
            }
        }

        [BsonIgnore]
        [JsonConverter(typeof(StringEnumConverter))]
        public ResultStatus Medication2_Status
        {
            get
            {
                if (Medication2_ID == null || Medication2_ID.Model == null || Medication2_Accept == null || !Medication2_Accept.Model.HasValue || Medication2_Refuse == null || !Medication2_Refuse.Model.HasValue ||
                    Medication2_Refuse_Discontinued == null || !Medication2_Refuse_Discontinued.Model.HasValue || Medication2_Refuse_Overstock == null || !Medication2_Refuse_Overstock.Model.HasValue ||
                    Medication2_Refuse_Unknown == null || !Medication2_Refuse_Unknown.Model.HasValue)
                {
                    return ResultStatus.Error;
                }
                if ((Medication2_Accept.Model.Value && Medication2_Refuse.Model.Value) ||
                    (!Medication2_Accept.Model.Value && !Medication2_Refuse.Model.Value && !(Form_Refuse_Deceased.Model.Value || Form_Refuse_TempAbsence.Model.Value || Form_Refuse_DaVitaDischarge.Model.Value)) ||
                    (Medication2_Accept.Model.Value && (Medication2_Refuse_Discontinued.Model.Value || Medication2_Refuse_Overstock.Model.Value || Medication2_Refuse_Unknown.Model.Value)) ||
                    (Medication2_Refuse.Model.Value && !(Medication2_Refuse_Discontinued.Model.Value || Medication2_Refuse_Overstock.Model.Value || Medication2_Refuse_Unknown.Model.Value)))
                {
                    return ResultStatus.ValidationWarning;
                }
                return ResultStatus.Success;
            }
        }

        [BsonIgnore]
        [JsonConverter(typeof(StringEnumConverter))]
        public ResultStatus Medication3_Status
        {
            get
            {
                if (Medication3_ID == null || Medication3_ID.Model == null || Medication3_Accept == null || !Medication3_Accept.Model.HasValue || Medication3_Refuse == null || !Medication3_Refuse.Model.HasValue ||
                    Medication3_Refuse_Discontinued == null || !Medication3_Refuse_Discontinued.Model.HasValue || Medication3_Refuse_Overstock == null || !Medication3_Refuse_Overstock.Model.HasValue ||
                    Medication3_Refuse_Unknown == null || !Medication3_Refuse_Unknown.Model.HasValue)
                {
                    return ResultStatus.Error;
                }
                if ((Medication3_Accept.Model.Value && Medication3_Refuse.Model.Value) ||
                    (!Medication3_Accept.Model.Value && !Medication3_Refuse.Model.Value && !(Form_Refuse_Deceased.Model.Value || Form_Refuse_TempAbsence.Model.Value || Form_Refuse_DaVitaDischarge.Model.Value)) ||
                    (Medication3_Accept.Model.Value && (Medication3_Refuse_Discontinued.Model.Value || Medication3_Refuse_Overstock.Model.Value || Medication3_Refuse_Unknown.Model.Value)) ||
                    (Medication3_Refuse.Model.Value && !(Medication3_Refuse_Discontinued.Model.Value || Medication3_Refuse_Overstock.Model.Value || Medication3_Refuse_Unknown.Model.Value)))
                {
                    return ResultStatus.ValidationWarning;
                }
                return ResultStatus.Success;
            }
        }


        [BsonIgnore]
        [JsonConverter(typeof(StringEnumConverter))]
        public ResultStatus Medication4_Status
        {
            get
            {
                if (Medication4_ID == null || Medication4_ID.Model == null || Medication4_Accept == null || !Medication4_Accept.Model.HasValue || Medication4_Refuse == null || !Medication4_Refuse.Model.HasValue ||
                    Medication4_Refuse_Discontinued == null || !Medication4_Refuse_Discontinued.Model.HasValue || Medication4_Refuse_Overstock == null || !Medication4_Refuse_Overstock.Model.HasValue ||
                    Medication4_Refuse_Unknown == null || !Medication4_Refuse_Unknown.Model.HasValue)
                {
                    return ResultStatus.Error;
                }
                if ((Medication4_Accept.Model.Value && Medication4_Refuse.Model.Value) ||
                    (!Medication4_Accept.Model.Value && !Medication4_Refuse.Model.Value && !(Form_Refuse_Deceased.Model.Value || Form_Refuse_TempAbsence.Model.Value || Form_Refuse_DaVitaDischarge.Model.Value)) ||
                    (Medication4_Accept.Model.Value && (Medication4_Refuse_Discontinued.Model.Value || Medication4_Refuse_Overstock.Model.Value || Medication4_Refuse_Unknown.Model.Value)) ||
                    (Medication4_Refuse.Model.Value && !(Medication4_Refuse_Discontinued.Model.Value || Medication4_Refuse_Overstock.Model.Value || Medication4_Refuse_Unknown.Model.Value)))
                {
                    return ResultStatus.ValidationWarning;
                }
                return ResultStatus.Success;
            }
        }

        [BsonIgnore]
        [JsonConverter(typeof(StringEnumConverter))]
        public ResultStatus Medication5_Status
        {
            get
            {
                if (Medication5_ID == null || Medication5_ID.Model == null || Medication5_Accept == null || !Medication5_Accept.Model.HasValue || Medication5_Refuse == null || !Medication5_Refuse.Model.HasValue ||
                    Medication5_Refuse_Discontinued == null || !Medication5_Refuse_Discontinued.Model.HasValue || Medication5_Refuse_Overstock == null || !Medication5_Refuse_Overstock.Model.HasValue ||
                    Medication5_Refuse_Unknown == null || !Medication5_Refuse_Unknown.Model.HasValue)
                {
                    return ResultStatus.Error;
                }
                if ((Medication5_Accept.Model.Value && Medication5_Refuse.Model.Value) ||
                    (!Medication5_Accept.Model.Value && !Medication5_Refuse.Model.Value && !(Form_Refuse_Deceased.Model.Value || Form_Refuse_TempAbsence.Model.Value || Form_Refuse_DaVitaDischarge.Model.Value)) ||
                    (Medication5_Accept.Model.Value && (Medication5_Refuse_Discontinued.Model.Value || Medication5_Refuse_Overstock.Model.Value || Medication5_Refuse_Unknown.Model.Value)) ||
                    (Medication5_Refuse.Model.Value && !(Medication5_Refuse_Discontinued.Model.Value || Medication5_Refuse_Overstock.Model.Value || Medication5_Refuse_Unknown.Model.Value)))
                {
                    return ResultStatus.ValidationWarning;
                }
                return ResultStatus.Success;
            }
        }
        
        [BsonIgnore]
        [JsonConverter(typeof(StringEnumConverter))]
        public ResultStatus Medication6_Status
        {
            get
            {
                if (Medication6_ID == null || Medication6_ID.Model == null || Medication6_Accept == null || !Medication6_Accept.Model.HasValue || Medication6_Refuse == null || !Medication6_Refuse.Model.HasValue ||
                    Medication6_Refuse_Discontinued == null || !Medication6_Refuse_Discontinued.Model.HasValue || Medication6_Refuse_Overstock == null || !Medication6_Refuse_Overstock.Model.HasValue ||
                    Medication6_Refuse_Unknown == null || !Medication6_Refuse_Unknown.Model.HasValue)
                {
                    return ResultStatus.Error;
                }
                if ((Medication6_Accept.Model.Value && Medication6_Refuse.Model.Value) ||
                    (!Medication6_Accept.Model.Value && !Medication6_Refuse.Model.Value && !(Form_Refuse_Deceased.Model.Value || Form_Refuse_TempAbsence.Model.Value || Form_Refuse_DaVitaDischarge.Model.Value)) ||
                    (Medication6_Accept.Model.Value && (Medication6_Refuse_Discontinued.Model.Value || Medication6_Refuse_Overstock.Model.Value || Medication6_Refuse_Unknown.Model.Value)) ||
                    (Medication6_Refuse.Model.Value && !(Medication6_Refuse_Discontinued.Model.Value || Medication6_Refuse_Overstock.Model.Value || Medication6_Refuse_Unknown.Model.Value)))
                {
                    return ResultStatus.ValidationWarning;
                }
                return ResultStatus.Success;
            }
        }


        [BsonIgnore]
        [JsonConverter(typeof(StringEnumConverter))]
        public ResultStatus Medication7_Status
        {
            get
            {
                if (Medication7_ID == null || Medication7_ID.Model == null || Medication7_Accept == null || !Medication7_Accept.Model.HasValue || Medication7_Refuse == null || !Medication7_Refuse.Model.HasValue ||
                    Medication7_Refuse_Discontinued == null || !Medication7_Refuse_Discontinued.Model.HasValue || Medication7_Refuse_Overstock == null || !Medication7_Refuse_Overstock.Model.HasValue ||
                    Medication7_Refuse_Unknown == null || !Medication7_Refuse_Unknown.Model.HasValue)
                {
                    return ResultStatus.Error;
                }
                if ((Medication7_Accept.Model.Value && Medication7_Refuse.Model.Value) ||
                    (!Medication7_Accept.Model.Value && !Medication7_Refuse.Model.Value && !(Form_Refuse_Deceased.Model.Value || Form_Refuse_TempAbsence.Model.Value || Form_Refuse_DaVitaDischarge.Model.Value)) ||
                    (Medication7_Accept.Model.Value && (Medication7_Refuse_Discontinued.Model.Value || Medication7_Refuse_Overstock.Model.Value || Medication7_Refuse_Unknown.Model.Value)) ||
                    (Medication7_Refuse.Model.Value && !(Medication7_Refuse_Discontinued.Model.Value || Medication7_Refuse_Overstock.Model.Value || Medication7_Refuse_Unknown.Model.Value)))
                {
                    return ResultStatus.ValidationWarning;
                }
                return ResultStatus.Success;
            }
        }
        
        [BsonIgnore]
        [JsonConverter(typeof(StringEnumConverter))]
        public ResultStatus Medication8_Status
        {
            get
            {
                if (Medication8_ID == null || Medication8_ID.Model == null || Medication8_Accept == null || !Medication8_Accept.Model.HasValue || Medication8_Refuse == null || !Medication8_Refuse.Model.HasValue ||
                    Medication8_Refuse_Discontinued == null || !Medication8_Refuse_Discontinued.Model.HasValue || Medication8_Refuse_Overstock == null || !Medication8_Refuse_Overstock.Model.HasValue ||
                    Medication8_Refuse_Unknown == null || !Medication8_Refuse_Unknown.Model.HasValue)
                {
                    return ResultStatus.Error;
                }
                if ((Medication8_Accept.Model.Value && Medication8_Refuse.Model.Value) ||
                    (!Medication8_Accept.Model.Value && !Medication8_Refuse.Model.Value && !(Form_Refuse_Deceased.Model.Value || Form_Refuse_TempAbsence.Model.Value || Form_Refuse_DaVitaDischarge.Model.Value)) ||
                    (Medication8_Accept.Model.Value && (Medication8_Refuse_Discontinued.Model.Value || Medication8_Refuse_Overstock.Model.Value || Medication8_Refuse_Unknown.Model.Value)) ||
                    (Medication8_Refuse.Model.Value && !(Medication8_Refuse_Discontinued.Model.Value || Medication8_Refuse_Overstock.Model.Value || Medication8_Refuse_Unknown.Model.Value)))
                {
                    return ResultStatus.ValidationWarning;
                }
                return ResultStatus.Success;
            }
        }
        
        public ProcessingResult(AutoFormsRunResult result, string path, int overallConfidence)
        {
            Path = path;

            OverallConfidence = overallConfidence;

            TimeStamp = DateTime.Now;

            if (result != null && result.FormFields.Count > 0)
            {
                foreach (FormField field in result.FormFields[0])
                {
                    if (field.Name.Equals("FormIdentifier"))
                    {
                        FormIdentifier = new ElementResult<string>()
                        {
                            Model = (field.Result as BarcodeFormFieldResult).BarcodeData.Count >= 1 ? GetDataString((field.Result as BarcodeFormFieldResult).BarcodeData[0].GetData()) : null,
                        };
                    }
                    if (field.Name.Equals("OrderNumber"))
                    {
                        OrderNumber = new ElementResult<string>()
                        {
                            Confidence = (field.Result as TextFormFieldResult).AverageConfidence,

                            Model = (field.Result as TextFormFieldResult).Text.Replace("\r\n", string.Empty)
                        };
                    }
                    if (field.Name.Equals("Form_Refuse_Deceased"))
                    {
                        Form_Refuse_Deceased = new ElementResult<bool?>()
                        {
                            Confidence = (field.Result as OmrFormFieldResult).AverageConfidence,

                            Model = (field.Result as OmrFormFieldResult).Text.Equals("1")
                        };
                    }
                    if (field.Name.Equals("Form_Refuse_DaVitaDischarge"))
                    {
                        Form_Refuse_DaVitaDischarge = new ElementResult<bool?>()
                        {
                            Confidence = (field.Result as OmrFormFieldResult).AverageConfidence,

                            Model = (field.Result as OmrFormFieldResult).Text.Equals("1")
                        };
                    }
                    if (field.Name.Equals("Form_Refuse_TempAbsence"))
                    {
                        Form_Refuse_TempAbsence = new ElementResult<bool?>()
                        {
                            Confidence = (field.Result as OmrFormFieldResult).AverageConfidence,

                            Model = (field.Result as OmrFormFieldResult).Text.Equals("1")
                        };
                    }
                    if (field.Name.Equals("Medication1_ID"))
                    {
                        Medication1_ID = new ElementResult<string>()
                        {
                            Confidence = (field.Result as TextFormFieldResult).AverageConfidence,

                            Model = (field.Result as TextFormFieldResult).Text.Replace("\r\n", string.Empty)
                        };
                    }
                    if (field.Name.Equals("Medication1_Accept"))
                    {
                        Medication1_Accept = new ElementResult<bool?>()
                        {
                            Confidence = (field.Result as OmrFormFieldResult).AverageConfidence,

                            Model = (field.Result as OmrFormFieldResult).Text.Equals("1")
                        };
                    }
                    if (field.Name.Equals("Medication1_Refuse"))
                    {
                        Medication1_Refuse = new ElementResult<bool?>()
                        {
                            Confidence = (field.Result as OmrFormFieldResult).AverageConfidence,

                            Model = (field.Result as OmrFormFieldResult).Text.Equals("1")
                        };
                    }
                    if (field.Name.Equals("Medication1_Refuse_Discontinued"))
                    {
                        Medication1_Refuse_Discontinued = new ElementResult<bool?>()
                        {
                            Confidence = (field.Result as OmrFormFieldResult).AverageConfidence,

                            Model = (field.Result as OmrFormFieldResult).Text.Equals("1")
                        };
                    }
                    if (field.Name.Equals("Medication1_Refuse_Overstock"))
                    {
                        Medication1_Refuse_Overstock = new ElementResult<bool?>()
                        {
                            Confidence = (field.Result as OmrFormFieldResult).AverageConfidence,

                            Model = (field.Result as OmrFormFieldResult).Text.Equals("1")
                        };
                    }
                    if (field.Name.Equals("Medication1_Refuse_Unknown"))
                    {
                        Medication1_Refuse_Unknown = new ElementResult<bool?>()
                        {
                            Confidence = (field.Result as OmrFormFieldResult).AverageConfidence,

                            Model = (field.Result as OmrFormFieldResult).Text.Equals("1")
                        };
                    }
                    if (field.Name.Equals("Medication2_ID"))
                    {
                        Medication2_ID = new ElementResult<string>()
                        {
                            Confidence = (field.Result as TextFormFieldResult).AverageConfidence,

                            Model = (field.Result as TextFormFieldResult).Text.Replace("\r\n", string.Empty)
                        };
                    }
                    if (field.Name.Equals("Medication2_Accept"))
                    {
                        Medication2_Accept = new ElementResult<bool?>()
                        {
                            Confidence = (field.Result as OmrFormFieldResult).AverageConfidence,

                            Model = (field.Result as OmrFormFieldResult).Text.Equals("1")
                        };
                    }
                    if (field.Name.Equals("Medication2_Refuse"))
                    {
                        Medication2_Refuse = new ElementResult<bool?>()
                        {
                            Confidence = (field.Result as OmrFormFieldResult).AverageConfidence,

                            Model = (field.Result as OmrFormFieldResult).Text.Equals("1")
                        };
                    }
                    if (field.Name.Equals("Medication2_Refuse_Discontinued"))
                    {
                        Medication2_Refuse_Discontinued = new ElementResult<bool?>()
                        {
                            Confidence = (field.Result as OmrFormFieldResult).AverageConfidence,

                            Model = (field.Result as OmrFormFieldResult).Text.Equals("1")
                        };
                    }
                    if (field.Name.Equals("Medication2_Refuse_Overstock"))
                    {
                        Medication2_Refuse_Overstock = new ElementResult<bool?>()
                        {
                            Confidence = (field.Result as OmrFormFieldResult).AverageConfidence,

                            Model = (field.Result as OmrFormFieldResult).Text.Equals("1")
                        };
                    }
                    if (field.Name.Equals("Medication2_Refuse_Unknown"))
                    {
                        Medication2_Refuse_Unknown = new ElementResult<bool?>()
                        {
                            Confidence = (field.Result as OmrFormFieldResult).AverageConfidence,

                            Model = (field.Result as OmrFormFieldResult).Text.Equals("1")
                        };
                    }
                    if (field.Name.Equals("Medication3_ID"))
                    {
                        Medication3_ID = new ElementResult<string>()
                        {
                            Confidence = (field.Result as TextFormFieldResult).AverageConfidence,

                            Model = (field.Result as TextFormFieldResult).Text.Replace("\r\n", string.Empty)
                        };
                    }
                    if (field.Name.Equals("Medication3_Accept"))
                    {
                        Medication3_Accept = new ElementResult<bool?>()
                        {
                            Confidence = (field.Result as OmrFormFieldResult).AverageConfidence,

                            Model = (field.Result as OmrFormFieldResult).Text.Equals("1")
                        };
                    }
                    if (field.Name.Equals("Medication3_Refuse"))
                    {
                        Medication3_Refuse = new ElementResult<bool?>()
                        {
                            Confidence = (field.Result as OmrFormFieldResult).AverageConfidence,

                            Model = (field.Result as OmrFormFieldResult).Text.Equals("1")
                        };
                    }
                    if (field.Name.Equals("Medication3_Refuse_Discontinued"))
                    {
                        Medication3_Refuse_Discontinued = new ElementResult<bool?>()
                        {
                            Confidence = (field.Result as OmrFormFieldResult).AverageConfidence,

                            Model = (field.Result as OmrFormFieldResult).Text.Equals("1")
                        };
                    }
                    if (field.Name.Equals("Medication3_Refuse_Overstock"))
                    {
                        Medication3_Refuse_Overstock = new ElementResult<bool?>()
                        {
                            Confidence = (field.Result as OmrFormFieldResult).AverageConfidence,

                            Model = (field.Result as OmrFormFieldResult).Text.Equals("1")
                        };
                    }
                    if (field.Name.Equals("Medication3_Refuse_Unknown"))
                    {
                        Medication3_Refuse_Unknown = new ElementResult<bool?>()
                        {
                            Confidence = (field.Result as OmrFormFieldResult).AverageConfidence,

                            Model = (field.Result as OmrFormFieldResult).Text.Equals("1")
                        };
                    }
                    if (field.Name.Equals("Medication4_ID"))
                    {
                        Medication4_ID = new ElementResult<string>()
                        {
                            Confidence = (field.Result as TextFormFieldResult).AverageConfidence,

                            Model = (field.Result as TextFormFieldResult).Text.Replace("\r\n", string.Empty)
                        };
                    }
                    if (field.Name.Equals("Medication4_Accept"))
                    {
                        Medication4_Accept = new ElementResult<bool?>()
                        {
                            Confidence = (field.Result as OmrFormFieldResult).AverageConfidence,

                            Model = (field.Result as OmrFormFieldResult).Text.Equals("1")
                        };
                    }
                    if (field.Name.Equals("Medication4_Refuse"))
                    {
                        Medication4_Refuse = new ElementResult<bool?>()
                        {
                            Confidence = (field.Result as OmrFormFieldResult).AverageConfidence,

                            Model = (field.Result as OmrFormFieldResult).Text.Equals("1")
                        };
                    }
                    if (field.Name.Equals("Medication4_Refuse_Discontinued"))
                    {
                        Medication4_Refuse_Discontinued = new ElementResult<bool?>()
                        {
                            Confidence = (field.Result as OmrFormFieldResult).AverageConfidence,

                            Model = (field.Result as OmrFormFieldResult).Text.Equals("1")
                        };
                    }
                    if (field.Name.Equals("Medication4_Refuse_Overstock"))
                    {
                        Medication4_Refuse_Overstock = new ElementResult<bool?>()
                        {
                            Confidence = (field.Result as OmrFormFieldResult).AverageConfidence,

                            Model = (field.Result as OmrFormFieldResult).Text.Equals("1")
                        };
                    }
                    if (field.Name.Equals("Medication4_Refuse_Unknown"))
                    {
                        Medication4_Refuse_Unknown = new ElementResult<bool?>()
                        {
                            Confidence = (field.Result as OmrFormFieldResult).AverageConfidence,

                            Model = (field.Result as OmrFormFieldResult).Text.Equals("1")
                        };
                    }
                    if (field.Name.Equals("Medication5_ID"))
                    {
                        Medication5_ID = new ElementResult<string>()
                        {
                            Confidence = (field.Result as TextFormFieldResult).AverageConfidence,

                            Model = (field.Result as TextFormFieldResult).Text.Replace("\r\n", string.Empty)
                        };
                    }
                    if (field.Name.Equals("Medication5_Accept"))
                    {
                        Medication5_Accept = new ElementResult<bool?>()
                        {
                            Confidence = (field.Result as OmrFormFieldResult).AverageConfidence,

                            Model = (field.Result as OmrFormFieldResult).Text.Equals("1")
                        };
                    }
                    if (field.Name.Equals("Medication5_Refuse"))
                    {
                        Medication5_Refuse = new ElementResult<bool?>()
                        {
                            Confidence = (field.Result as OmrFormFieldResult).AverageConfidence,

                            Model = (field.Result as OmrFormFieldResult).Text.Equals("1")
                        };
                    }
                    if (field.Name.Equals("Medication5_Refuse_Discontinued"))
                    {
                        Medication5_Refuse_Discontinued = new ElementResult<bool?>()
                        {
                            Confidence = (field.Result as OmrFormFieldResult).AverageConfidence,

                            Model = (field.Result as OmrFormFieldResult).Text.Equals("1")
                        };
                    }
                    if (field.Name.Equals("Medication5_Refuse_Overstock"))
                    {
                        Medication5_Refuse_Overstock = new ElementResult<bool?>()
                        {
                            Confidence = (field.Result as OmrFormFieldResult).AverageConfidence,

                            Model = (field.Result as OmrFormFieldResult).Text.Equals("1")
                        };
                    }
                    if (field.Name.Equals("Medication5_Refuse_Unknown"))
                    {
                        Medication5_Refuse_Unknown = new ElementResult<bool?>()
                        {
                            Confidence = (field.Result as OmrFormFieldResult).AverageConfidence,

                            Model = (field.Result as OmrFormFieldResult).Text.Equals("1")
                        };
                    }
                    if (field.Name.Equals("Medication6_ID"))
                    {
                        Medication6_ID = new ElementResult<string>()
                        {
                            Confidence = (field.Result as TextFormFieldResult).AverageConfidence,

                            Model = (field.Result as TextFormFieldResult).Text.Replace("\r\n", string.Empty)
                        };
                    }
                    if (field.Name.Equals("Medication6_Accept"))
                    {
                        Medication6_Accept = new ElementResult<bool?>()
                        {
                            Confidence = (field.Result as OmrFormFieldResult).AverageConfidence,

                            Model = (field.Result as OmrFormFieldResult).Text.Equals("1")
                        };
                    }
                    if (field.Name.Equals("Medication6_Refuse"))
                    {
                        Medication6_Refuse = new ElementResult<bool?>()
                        {
                            Confidence = (field.Result as OmrFormFieldResult).AverageConfidence,

                            Model = (field.Result as OmrFormFieldResult).Text.Equals("1")
                        };
                    }
                    if (field.Name.Equals("Medication6_Refuse_Discontinued"))
                    {
                        Medication6_Refuse_Discontinued = new ElementResult<bool?>()
                        {
                            Confidence = (field.Result as OmrFormFieldResult).AverageConfidence,

                            Model = (field.Result as OmrFormFieldResult).Text.Equals("1")
                        };
                    }
                    if (field.Name.Equals("Medication6_Refuse_Overstock"))
                    {
                        Medication6_Refuse_Overstock = new ElementResult<bool?>()
                        {
                            Confidence = (field.Result as OmrFormFieldResult).AverageConfidence,

                            Model = (field.Result as OmrFormFieldResult).Text.Equals("1")
                        };
                    }
                    if (field.Name.Equals("Medication6_Refuse_Unknown"))
                    {
                        Medication6_Refuse_Unknown = new ElementResult<bool?>()
                        {
                            Confidence = (field.Result as OmrFormFieldResult).AverageConfidence,

                            Model = (field.Result as OmrFormFieldResult).Text.Equals("1")
                        };
                    }
                    if (field.Name.Equals("Medication7_ID"))
                    {
                        Medication7_ID = new ElementResult<string>()
                        {
                            Confidence = (field.Result as TextFormFieldResult).AverageConfidence,

                            Model = (field.Result as TextFormFieldResult).Text.Replace("\r\n", string.Empty)
                        };
                    }
                    if (field.Name.Equals("Medication7_Accept"))
                    {
                        Medication7_Accept = new ElementResult<bool?>()
                        {
                            Confidence = (field.Result as OmrFormFieldResult).AverageConfidence,

                            Model = (field.Result as OmrFormFieldResult).Text.Equals("1")
                        };
                    }
                    if (field.Name.Equals("Medication7_Refuse"))
                    {
                        Medication7_Refuse = new ElementResult<bool?>()
                        {
                            Confidence = (field.Result as OmrFormFieldResult).AverageConfidence,

                            Model = (field.Result as OmrFormFieldResult).Text.Equals("1")
                        };
                    }
                    if (field.Name.Equals("Medication7_Refuse_Discontinued"))
                    {
                        Medication7_Refuse_Discontinued = new ElementResult<bool?>()
                        {
                            Confidence = (field.Result as OmrFormFieldResult).AverageConfidence,

                            Model = (field.Result as OmrFormFieldResult).Text.Equals("1")
                        };
                    }
                    if (field.Name.Equals("Medication7_Refuse_Overstock"))
                    {
                        Medication7_Refuse_Overstock = new ElementResult<bool?>()
                        {
                            Confidence = (field.Result as OmrFormFieldResult).AverageConfidence,

                            Model = (field.Result as OmrFormFieldResult).Text.Equals("1")
                        };
                    }
                    if (field.Name.Equals("Medication7_Refuse_Unknown"))
                    {
                        Medication7_Refuse_Unknown = new ElementResult<bool?>()
                        {
                            Confidence = (field.Result as OmrFormFieldResult).AverageConfidence,

                            Model = (field.Result as OmrFormFieldResult).Text.Equals("1")
                        };
                    }
                    if (field.Name.Equals("Medication8_ID"))
                    {
                        Medication8_ID = new ElementResult<string>()
                        {
                            Confidence = (field.Result as TextFormFieldResult).AverageConfidence,

                            Model = (field.Result as TextFormFieldResult).Text.Replace("\r\n", string.Empty)
                        };
                    }
                    if (field.Name.Equals("Medication8_Accept"))
                    {
                        Medication8_Accept = new ElementResult<bool?>()
                        {
                            Confidence = (field.Result as OmrFormFieldResult).AverageConfidence,

                            Model = (field.Result as OmrFormFieldResult).Text.Equals("1")
                        };
                    }
                    if (field.Name.Equals("Medication8_Refuse"))
                    {
                        Medication8_Refuse = new ElementResult<bool?>()
                        {
                            Confidence = (field.Result as OmrFormFieldResult).AverageConfidence,

                            Model = (field.Result as OmrFormFieldResult).Text.Equals("1")
                        };
                    }
                    if (field.Name.Equals("Medication8_Refuse_Discontinued"))
                    {
                        Medication8_Refuse_Discontinued = new ElementResult<bool?>()
                        {
                            Confidence = (field.Result as OmrFormFieldResult).AverageConfidence,

                            Model = (field.Result as OmrFormFieldResult).Text.Equals("1")
                        };
                    }
                    if (field.Name.Equals("Medication8_Refuse_Overstock"))
                    {
                        Medication8_Refuse_Overstock = new ElementResult<bool?>()
                        {
                            Confidence = (field.Result as OmrFormFieldResult).AverageConfidence,

                            Model = (field.Result as OmrFormFieldResult).Text.Equals("1")
                        };
                    }
                    if (field.Name.Equals("Medication8_Refuse_Unknown"))
                    {
                        Medication8_Refuse_Unknown = new ElementResult<bool?>()
                        {
                            Confidence = (field.Result as OmrFormFieldResult).AverageConfidence,

                            Model = (field.Result as OmrFormFieldResult).Text.Equals("1")
                        };
                    }

                }
            }
        }

        private string GetDataString(byte[] data)
        {
            string result = string.Empty;

            for (int i = 0; i < data.Length; i++)
            {
                result = result + System.Convert.ToChar(data[i]).ToString();
            }

            return result;
        }
    }
}
