﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataReconPoC.Common
{
    public class ElementResult<T>
    {
        public T Model { get; set; }

        public int? Confidence { get; set; }
    }
}
