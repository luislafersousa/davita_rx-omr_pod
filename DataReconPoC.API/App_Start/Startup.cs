﻿using System.Threading.Tasks;
using System.Web.Cors;
using System.Web.Http;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Owin;

[assembly: OwinStartup(typeof(DataReconPoC.API.App_Start.Startup))]
namespace DataReconPoC.API.App_Start
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureCors(app);

            ConfigureHTTP(app);
        }

        private void ConfigureCors(IAppBuilder app)
        {
            var policy = new CorsPolicy
            {
                SupportsCredentials = true
            };

            policy.Origins.Add("http://localhost:8080");
            policy.Headers.Add("GET, POST, PUT, DELETE, OPTIONS");
            policy.Methods.Add("Content-Type, Authorization, Content-Length, X-Requested-With");

            app.UseCors(new CorsOptions
            {
                PolicyProvider = new CorsPolicyProvider
                {
                    PolicyResolver = context => Task.FromResult(policy)
                }
            });
        }

        private void ConfigureHTTP(IAppBuilder app)
        {
            HttpConfiguration config = new HttpConfiguration();

            config.MapHttpAttributeRoutes();

            app.UseWebApi(config);
        }
    }
}
