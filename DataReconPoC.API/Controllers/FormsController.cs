﻿using DataReconPoC.Common;
using DataReconPoC.Data;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace DataReconPoC.API.Controllers
{
    [RoutePrefix("forms")]
    public class FormsController : ApiController
    {
        private Repository _repository;

        public FormsController()
        {
            _repository = new Repository();
        }

        [HttpGet]
        [Route("")]
        public async Task<IHttpActionResult> Get()
        {
            return Ok(await _repository.Get());
        }
        
        [HttpGet]
        [Route("image/{id}")]
        public async Task<HttpResponseMessage> GetImage(string id)
        {
            var forms = await _repository.Get();
            var form = forms.FirstOrDefault(f => f.Id == id);

            if (form != null)
            {
                HttpResponseMessage response = new HttpResponseMessage();
                response.Content = new StreamContent(new FileStream(form.Path, FileMode.Open)); 
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("image/jpg");

                return response;
            }

            return null;
        }
    }
}
