﻿using DataReconPoC.Common;
using MongoDB.Bson;
using MongoDB.Bson.IO;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using MongoDB.Bson.Serialization;

namespace DataReconPoC.Data
{
    public class Repository
    {
        private MongoClient _client;

        private string _dbName = "dataReconciliation";

        private string _processingResultsCollectionName = "processingResults";

        public Repository()
        {
            _client = new MongoClient();
        }

        public async Task Save(IList<ProcessingResult> data) 
        {
            var database = _client.GetDatabase(_dbName);

            var count = (await Get()).Count;

            var collection = database.GetCollection<BsonDocument>(_processingResultsCollectionName);

            var documents = new List<BsonDocument>();

            var counter = 0;

            foreach (ProcessingResult res in data)
            {
                counter++;
                res.Id = (count + counter).ToString();
                var document = res.ToBsonDocument<ProcessingResult>();
                documents.Add(document);
            }

            if (documents.Count > 0)
            {
                await collection.InsertManyAsync(documents);
            }
        }

        public async Task<IList<ProcessingResult>> Get()
        {
            var database = _client.GetDatabase(_dbName);

            var collection = database.GetCollection<BsonDocument>(_processingResultsCollectionName);

            var results = new List<ProcessingResult>();

            var documents = await collection.Find(new BsonDocument()).ToListAsync();

            foreach (BsonDocument document in documents) 
            {
                var res = BsonSerializer.Deserialize<ProcessingResult>(document);

                results.Add(res);
            }

            return results;
        }
    }
}
