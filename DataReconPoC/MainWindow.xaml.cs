﻿using DataReconPoC.Common;
using DataReconPoC.Data;
using DataReconPoC.Processing;
using Leadtools.Forms.Recognition;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DataReconPoC
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Repository _repository;

        private string _defaultMasterPath = @"\\Mac\Box Sync\DavitaRX\DaVita RX PoD Data Recon\00 - Discovery\Test Files\LeadTools Recognition SDK\TestTemplate";

        private string _defaultFormPath = @"\\Mac\Box Sync\DavitaRX\DaVita RX PoD Data Recon\00 - Discovery\Test Scans\PoC Testing";

        public MainWindow()
        {
            InitializeComponent();

            _masterTemplateDirectoryTextbox.Text = _defaultMasterPath;

            _formDirectoryTextbox.Text = _defaultFormPath;

            _repository = new Repository();
        }

        private void _masterTemplateDirectoryBrowseButton_Click(object sender, RoutedEventArgs e)
        {
            var fileDialog = new System.Windows.Forms.FolderBrowserDialog();
            var result = fileDialog.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                _masterTemplateDirectoryTextbox.Text = fileDialog.SelectedPath;
            }
        }

        private void _formDirectoryButton_Click(object sender, RoutedEventArgs e)
        {
            var fileDialog = new System.Windows.Forms.FolderBrowserDialog();
            var result = fileDialog.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                _formDirectoryTextbox.Text = fileDialog.SelectedPath;
            }
        }

        private async void _processButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                _processButton.Visibility = Visibility.Collapsed;
                _loadingIndicator.Visibility = Visibility.Visible;

                var results = new List<ProcessingResult>();

                var repoPath = _masterTemplateDirectoryTextbox.Text;

                var formPath = _formDirectoryTextbox.Text;

                await Task.Run(() =>
                {
                    if (!Directory.Exists(formPath))
                    {
                        return;
                    }

                    var filesToProcess = Directory.GetFiles(formPath);
                    
                    var processor = new ProcessingHelper();
                    processor.Init(repoPath);

                    foreach (string file in filesToProcess)
                    {
                        if (file.EndsWith(".png"))
                        {
                            var result = processor.RecognizeForm(file);
                            results.Add(result);
                        }
                    }
                    
                    processor.CleanUp();
                });

                await _repository.Save(results);
            }
            finally
            {
                _processButton.Visibility = Visibility.Visible;
                _loadingIndicator.Visibility = Visibility.Collapsed;
            }
        }
    }
}
