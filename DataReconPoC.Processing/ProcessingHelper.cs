﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Leadtools;
using Leadtools.Codecs;
using Leadtools.Forms;
using Leadtools.Forms.Ocr;
using Leadtools.Forms.Processing;
using Leadtools.Forms.Recognition;
using Leadtools.Forms.Recognition.Barcode;
using Leadtools.Forms.Auto;
using System.IO;
using Leadtools.Barcode;
using Leadtools.ImageProcessing.Core;
using Leadtools.ImageProcessing;
using DataReconPoC.Common;

namespace DataReconPoC.Processing
{
    public class ProcessingHelper
    {
        private IOcrEngine _cleanUpOcrEngine;
        private AutoFormsEngine _autoEngine;
        private RasterCodecs _formsCodec;
        private IOcrEngine _ocrEngine;
        private DiskMasterFormsRepository _formsRepository;
        private BarcodeEngine _barcodeEngine;
        private RasterCodecs _rasterCodecs;

        public void Init(string formRepositoryPath)
        {
            try
            {
                string LICENSE_FILE = @"C:\LEADTOOLS 19\Common\License\LEADTOOLS.lic";

                string LICENSE_KEY_FILE = @"C:\LEADTOOLS 19\Common\License\LEADTOOLS.LIC.KEY";

                string LICENSE_KEY = System.IO.File.ReadAllText(LICENSE_KEY_FILE);

                RasterSupport.SetLicense(LICENSE_FILE, LICENSE_KEY);

                _formsCodec = new RasterCodecs();

                // Create an OCR Engine for each processor on the machine. This allows for optimal use of thread during recognition and processing.
                _ocrEngine = OcrEngineManager.CreateEngine(OcrEngineType.Advantage, false);
                _ocrEngine.Startup(_formsCodec, null, null, @"C:\LEADTOOLS 19\Bin\Common\OcrAdvantageRuntime");

                SetEngineSettings(_ocrEngine);

                _cleanUpOcrEngine = OcrEngineManager.CreateEngine(OcrEngineType.Advantage, true);
                _cleanUpOcrEngine.Startup(null, null, null, null);

                _barcodeEngine = new BarcodeEngine();

                StartUpRasterCodecs();

                var managers = AutoFormsRecognitionManager.None;
                managers |= AutoFormsRecognitionManager.Ocr;

                //Point repository to directory with existing master forms
                _formsRepository = new DiskMasterFormsRepository(_formsCodec, formRepositoryPath);
                _autoEngine = new AutoFormsEngine(_formsRepository, _ocrEngine, _barcodeEngine, managers, 30, 80, true);
                _autoEngine.UseThreadPool = true;
            }
            catch (Exception ex)
            {
            }
        }


        private void StartUpRasterCodecs()
        {
            try
            {
                _rasterCodecs = new RasterCodecs();

                //To turn off the dithering method when converting colored images to 1-bit black and white image during the load
                //so the text in the image is not damaged.
                RasterDefaults.DitheringMethod = RasterDitheringMethod.None;

                //To ensure better results from OCR engine, set the loading resolution to 300 DPI 
                _rasterCodecs.Options.Load.Resolution = 300;
            }
            catch (Exception exp)
            {
            }
        }

        public ProcessingResult RecognizeForm(string pathToForm)
        {
            try
            {
                RasterImage image = _rasterCodecs.Load(pathToForm, 0, CodecsLoadByteOrder.Bgr, 1, 1);
                image.ChangeViewPerspective(RasterViewPerspective.TopLeft);
                var cleanedImage = CleanupImage(image, pathToForm);
                _autoEngine.RecognizeFirstPageOnly = true;
                AutoFormsRunResult runResult = _autoEngine.Run(cleanedImage, null, null, null);
                if (runResult != null)
                {
                    var processingResult = new ProcessingResult(runResult, pathToForm, runResult.RecognitionResult.Result.Confidence);
                    
                    return processingResult;
                }
            }
            catch (Exception ex)
            {
            }

            return new ProcessingResult(null, pathToForm, 0);
        }

        private void SetEngineSettings(IOcrEngine engine)
        {
            try
            {
                engine.SettingManager.SetEnumValue("Recognition.Fonts.DetectFontStyles", 0);
                engine.SettingManager.SetBooleanValue("Recognition.Fonts.RecognizeFontAttributes", false);

                if (engine.SettingManager.IsSettingNameSupported("Recognition.RecognitionModuleTradeoff"))
                {
                    engine.SettingManager.SetEnumValue("Recognition.RecognitionModuleTradeoff", "Accurate");
                }
            }
            catch (Exception ex)
            {
            }

        }

        private RasterImage CleanupImage(RasterImage imageToClean, string fileName)
        {
            var cleanedImage = imageToClean;

            try
            {
                if (_cleanUpOcrEngine != null && _cleanUpOcrEngine.IsStarted)
                {
                    using (IOcrDocument document = _cleanUpOcrEngine.DocumentManager.CreateDocument())
                    {
                        imageToClean.Page = 1;
                        var page = document.Pages.AddPage(imageToClean, null);
                        page.AutoPreprocess(OcrAutoPreprocessPageCommand.Deskew, null);
                        page.AutoPreprocess(OcrAutoPreprocessPageCommand.Invert, null);
                        page.AutoPreprocess(OcrAutoPreprocessPageCommand.Rotate, null);
                        cleanedImage = page.GetRasterImage();
                        document.Pages.Clear();
                    }
                }
            }
            catch (Exception ex)
            {
            }

            return cleanedImage;
        }


        public void CleanUp()
        {
            _autoEngine.Dispose();
            if (_ocrEngine != null && _ocrEngine.IsStarted)
            {
                _ocrEngine.Shutdown();
            }
            if (_cleanUpOcrEngine != null && _cleanUpOcrEngine.IsStarted)
            {
                _cleanUpOcrEngine.Shutdown();
                _cleanUpOcrEngine.Dispose();
            }
        }
    }
}
