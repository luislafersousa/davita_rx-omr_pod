using System.Web;
using System.Web.Routing;
using System.Web.WebPages;

namespace DataReconPoC.Web.Routing
{
    public class DefaultRouteHandler : IRouteHandler
    {
        public IHttpHandler GetHttpHandler(RequestContext requestContext)
        {
            return WebPageHttpHandler.CreateFromVirtualPath("~/dataRecon/index.cshtml");
        }
    }
}
