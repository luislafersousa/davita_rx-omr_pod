using System.Web.Routing;
using DataReconPoC.Web.Routing;

namespace DataReconPoC.Web
{

    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.Add("Default", new DefaultRoute());
        }
    }
}
