using System.Web.Optimization;

namespace DataReconPoC.Web
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/bundles/style/css/dataRecon")
                .Include("~/style/app.css"));

            bundles.Add(new StyleBundle("~/bundles/style/css/libraries")
                .IncludeDirectory("~/Style/css/", "*.css", true));

            bundles.Add(new ScriptBundle("~/bundles/js/dataRecon")
                .IncludeDirectory("~/dataRecon", "*.js", true));

            bundles.Add(new ScriptBundle("~/bundles/js/libraries")
                .Include("~/Scripts/jquery-2.0.3.js")
                .Include("~/Scripts/moment.js")
                .Include("~/Scripts/bootstrap.js")
                .Include("~/Scripts/angular.js")
                .Include("~/Scripts/angular-ui-router.js")
                .Include("~/Scripts/ui-bootstrap-tpls-0.14.0.js")
                .Include("~/Scripts/angularjs-dropdown-multiselect.js")
                .Include("~/Scripts/datetimepicker.js")
                .Include("~/Scripts/smart-table.js")
                .Include("~/Scripts/angular-ui-switch.js")
                .Include("~/Scripts/angular-bootstrap-lightbox.min.js")
                .Include("~/Scripts/angular-modal-service.js")
                .Include("~/Scripts/lodash.js"));
        }
    }
}