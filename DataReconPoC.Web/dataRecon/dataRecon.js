﻿'use strict';

var dataRecon = angular.module('dataRecon', ['ui.router', 'ui.bootstrap', 'bootstrapLightbox', 'ui.bootstrap.datetimepicker', 'angularjs-dropdown-multiselect', 'smart-table', 'uiSwitch', 'angularModalService'])

    .config(['$stateProvider', '$locationProvider', function ($stateProvider, $locationProvider) {
        
        $stateProvider
            .state('home', {
                url: '/',
                templateUrl: '/dataRecon/components/home/home',
                controller: 'homeController',
                controllerAs: 'home',
                title: 'Home'
            })
            .state('form', {
                url: '/form/:Id',
                templateUrl: '/dataRecon/components/form/form/',
                controller: 'formController',
                controllerAs: 'form',
                title: 'Form'
            })
            .state('403', {
                url: '/403',
                templateUrl: '/dataRecon/shared/pages/403',
                title: 'Access Denied'
            })
            .state('otherwise', {
                url: '*path',
                templateUrl: '/dataRecon/shared/pages/404',
                title: 'Page Not Found'
            });

        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });

    }])

    .run(['$templateCache', '$rootScope', '$state', '$stateParams', 'apiService', 'urls', function ($templateCache, $rootScope, $state, $stateParams, apiService, urls) {

        /// VARIABLES
        $rootScope.title = 'Data Recon';

        //<ui-view> contains a pre-rendered template for the current view
        //caching it will prevent a round-trip to a server at the first page load
        var view = angular.element('#ui-view');
        $templateCache.put(view.data('tmpl-url'), view.html());

        //allows to retrieve UI Router state information from inside templates
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;

        $rootScope.$on('$stateChangeSuccess', function (event, toState) {
            //sets the layout name, which can be used to display different layouts (header, footer etc.)
            //based on which page the user is located
            $rootScope.layout = toState.layout;

            if (toState.hasOwnProperty('title')) {
                $rootScope.title = "Data Recon | " + toState.title;
            }
            else
            {
                $rootScope.title = "Data Recon";
            }
        });
    }]);