﻿dataRecon.constant('urls', {
    forms: {
        get: 'forms/',
        image: 'forms/image/'
    }
});

dataRecon.constant('broadcasts', {
    newTopMessage: 'newTopMessage'
});