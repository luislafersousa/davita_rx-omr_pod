﻿'use strict';

dataRecon.factory('apiService', ['$http', '$q', '$cacheFactory', 'constants', 'topMessageService', function ($http, $q, $cacheFactory, constants, topMessageService) {

    ///PRIVATE FUNCTIONS
    var getUrl = function (relativeUrl, restParams, queryParams) {
        var params = '';
        if (restParams) {
            var trimmedParams = [];
            _.forEach(restParams, function (restParam) {
                var trimmedParam = _.trim(restParam);
                trimmedParams.push(trimmedParam);
            });

            params = trimmedParams.join('/');
        }

        if (queryParams) {
            if (params.substr(params.length - 1, 1) === '/') {
                params = params.substr(0, params.length - 1);
            }
            var trimmedParams = [];
            for (var key in queryParams) {
                if (queryParams[key]) {
                    var trimmedParam = _.trim(queryParams[key]);
                    trimmedParams.push(key + '=' + trimmedParam);
                }
            } 
            params = params + '?' + trimmedParams.join('&');
        }

        var url = constants.apiUrl + relativeUrl + params;

        return url;
    };

    /// PUBLIC INTERFACE
    return {
        getUrl: function(relativeUrl, restParams, queryParams) {
            var url = getUrl(relativeUrl, restParams, queryParams);
            return url;
        },
        invalidateCache: function(relativeUrl, restParams, queryParams) {
            var url = getUrl(relativeUrl, restParams, queryParams);
            $cacheFactory.get('$http').remove(url);
        },
        getRequest: function (relativeUrl, restParams, queryParams, doCache) {
            var url = getUrl(relativeUrl, restParams, queryParams);
            doCache = typeof doCache === 'undefined' ? true : doCache; //default caching value is true

            return $http.get(url, { cache: doCache, withCredentials: true })
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    if (response && response.data && response.data.ExceptionMessage) {
                        topMessageService.setTopMessage(topMessageService.types.danger, response.data.ExceptionMessage);
                    }
                    else {
                        topMessageService.setTopMessage(topMessageService.types.danger, "There was a problem with your request, please try again.");
                    }
                    return $q.reject(response.data);
                });
        },
        postRequest: function (relativeUrl, data, restParams, queryParams, doCache) {
            var url = getUrl(relativeUrl, restParams, queryParams);
            doCache = typeof doCache === 'undefined' ? true : doCache; //default caching value is true

            return $http.post(url, data, { cache: doCache, withCredentials: true })
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    if (response && response.data && response.data.ExceptionMessage) {
                        topMessageService.setTopMessage(topMessageService.types.danger, response.data.ExceptionMessage)
                    }
                    else {
                        topMessageService.setTopMessage(topMessageService.types.danger, "There was a problem, please try again.");
                    }
                    return $q.reject(response.data);
                });
        },
        putRequest: function (relativeUrl, data, restParams, queryParams, doCache) {
            var url = getUrl(relativeUrl, restParams, queryParams);
            doCache = typeof doCache === 'undefined' ? true : doCache; //default caching value is true

            return $http.put(url, data, { cache: doCache, withCredentials: true })
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    if (response && response.data && response.data.ExceptionMessage) {
                        topMessageService.setTopMessage(topMessageService.types.danger, response.data.ExceptionMessage)
                    }
                    else {
                        topMessageService.setTopMessage(topMessageService.types.danger, "There was a problem, please try again.");
                    }
                    return $q.reject(response.data);
                });
        }
    };
}]);