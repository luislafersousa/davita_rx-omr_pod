﻿'use strict';

dataRecon.factory('topMessageService', ['$rootScope', '$timeout', 'broadcasts', function ($rootScope, $timeout, broadcasts) {

    /// PRIVATE VARIABLES
    var topNotifications = [];

    var types = {
        danger: {
            class: 'alert-danger',
            title: 'Error!'
        },
        dismissable: {
            class: 'alert-dismissable',
            title: 'Info'
        },
        dismissible: {
            class: 'alert-dismissible',
            title: 'Info'
        },
        info: {
            class: 'alert-info',
            title: 'Danger!'
        },
        link: {
            class: 'alert-link',
            title: 'Info'
        },
        success: {
            class: 'alert-success',
            title: 'Success!'
        },
        warning: {
            class: 'alert-warning',
            title: 'Warning!'
        }
    };

    /// BROADCASTS
    $rootScope.$on('$stateChangeStart', function () {
        topNotifications = [];
        $rootScope.$broadcast(broadcasts.newTopMessage, topNotifications);
    });

    /// PUBLIC INTERFACE
    return {
        types: types,
        setTopMessage: function (type, message) {
            if (topNotifications.length < 3) {
                topNotifications.push({ class: type.class, title: type.title, message: message });
                $rootScope.$broadcast(broadcasts.newTopMessage, topNotifications);
            }
        }
    };
}]);