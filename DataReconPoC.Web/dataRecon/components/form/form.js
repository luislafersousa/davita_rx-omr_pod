﻿'use strict';

dataRecon.controller('formController', ['$stateParams', 'apiService', 'urls', 'ModalService', 'topMessageService', 'Lightbox', function ($stateParams, apiService, urls, modalService, topMessageService, Lightbox) {

    /// VARIABLES
    var controller = this;
    controller.result = {};
    controller.imagePath = apiService.getUrl(urls.forms.image, [$stateParams.Id]);

    /// FUNCTIONS
    controller.openLightboxModal = function () {
        Lightbox.openModal([controller.imagePath],0);
    };


    /// ONLOAD API CALLS
    apiService.getRequest(urls.forms.get)
    .then(function (data) {
        controller.result = _.find(data, function (result) {
            return result.Id === $stateParams.Id;
        });
    });
}]);