﻿'use strict';

// Path: /
dataRecon.controller('headerController', ['$scope', 'constants', 'broadcasts', 'apiService', 'urls', function ($scope, constants, broadcasts, apiService, urls ) {

    /// BROADCASTS
    $scope.$on(broadcasts.newTopMessage, function(event, data) {
        $scope.topNotifications = data;
    });
}]);

/// DIRECTIVES
dataRecon.directive('topMessage', function() {
    return {
        restrict: 'E',
        templateUrl: '/dataRecon/components/header/topMessageTemplate.cshtml',
        controller: 'headerController'
    };
});