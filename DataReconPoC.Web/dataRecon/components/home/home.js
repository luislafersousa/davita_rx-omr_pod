﻿'use strict';

dataRecon.controller('homeController', ['$location', '$stateParams', 'apiService', 'urls', 'ModalService', 'topMessageService', function ($location, $stateParams, apiService, urls, modalService, topMessageService) {
    var controller = this;

    /// VARIABLES
    var controller = this;
    controller.showFilters = false;
    controller.results = [];
    controller.visibleResults = [];

    /// FUNCTIONS
    controller.onClickFilter = function () {
        controller.showFilters = !controller.showFilters;
        controller.clearCustomFilters();
    };

    controller.clearCustomFilters = function () {
        if (controller.filters) {
            if (controller.filters.date) {
                controller.filters.date.before = null;
                controller.filters.date.after = null;
            }
            if (controller.filters.confidence) {
                controller.filters.confidence.lower = null;
                controller.filters.confidence.higher = null;
            }
        }
    };

    controller.viewResult = function (result) {
        $location.path('/form/'+result.Id);
    };

    /// ONLOAD API CALLS
    apiService.getRequest(urls.forms.get)
    .then(function (data) {
        controller.results = data;
        controller.visibleResults = controller.results;
    });
}]);