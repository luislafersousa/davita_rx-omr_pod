using Owin;

[assembly: Microsoft.Owin.OwinStartup(typeof(DataReconPoC.Web.Startup))]
namespace DataReconPoC.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll); 
        }
    }
}
